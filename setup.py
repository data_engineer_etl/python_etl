# setup.py
import os

# Chemins relatifs au dossier où se trouve setup.py
directories = [
    "data/raw", "data/processed", "notebook", 
    "src/etl", "src/etl/extract","src/etl/transform","src/etl/load", "src/models", "src/utils", "tests"
]

# Le chemin de base est le répertoire où se trouve setup.py
base_path = os.path.dirname(os.path.abspath(__file__))

for directory in directories:
    # Créer le chemin complet pour chaque dossier
    full_path = os.path.join(base_path, directory)
    os.makedirs(full_path, exist_ok=True)
