## Configuration Initiale du Projet

Pour configurer l'arborescence des dossiers nécessaire pour ce projet, un script `setup.py` est fourni. Ce script créera automatiquement les dossiers suivants : `data/raw`, `data/processed`, `notebook`, `src/etl`, `src/models`, `src/utils`, et `tests`.

### Prérequis

- Environnement virtuel python.
- Python 3.x installé sur votre machine.

### Étapes pour Exécuter le Script

1. Naviguez jusqu'au dossier `python_etl` où se trouve le script `setup.py`. 
   Vous pouvez le faire en utilisant la commande suivante dans votre terminal ou invite de commande :

   >>> bash
   >>> cd chemin/vers/python_etl
   # Executez le script 'setup.py' avec python. En utilisant la commande suivante:
   >>> python setup.py

   # Ce qui se Passe Ensuite
   Une fois le script exécuté, les dossiers nécessaires seront créés dans le dossier python_etl. Vous êtes maintenant prêt à travailler sur le projet.


