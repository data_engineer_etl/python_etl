from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import sys
import os


# Ajout du chemin vers le dossier src/etl/ pour pouvoir importer les modules
#sys.path.append('../src/')

# Import des modules
from etl.extract import load_csv_file
from etl.transform import find_mention
from etl.load import generate_output_file

# Définition des arguments par défaut
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2023, 11, 21),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

# Définition du DAG
dag = DAG(
    'etl_pipeline_dag',
    default_args=default_args,
    description='Un pipeline ETL pour le traitement des données médicales',
    schedule_interval=timedelta(days=1),
)

# Définition des tâches
# Extraction des données csv
def extract_csv_data():
    drugs = load_csv_file('../data/intermediate/drugs.csv')
    pubmed = load_csv_file('../data/intermediate/pubmed.csv')
    clinical_trials = load_csv_file('../data/intermediate/clinical_trials.csv')
    return drugs, pubmed, clinical_trials


# Transformation des données
def transform_csv_data(tr):
    drugs, pubmed, clinical_trials = tr.xcom_pull(task_ids='extract_csv_data') # on récupère les données extraites
    pubmed_mentions = find_mention(pubmed, drugs) # on applique la transformation
    clinical_trials_mentions = find_mention(clinical_trials, drugs)
    mentions = pubmed_mentions + clinical_trials_mentions
    return mentions

def load_csv_data(ld):
    mentions = ld.xcom_pull(task_ids='transform_csv_data') # on récupère les données transformées
    generate_output_file(mentions, '../data/gold/mentions.json') # on applique le chargement dans la gold

# Mise en place des différentes tâches
# Extraction des données csv
extract_csv_data_task = PythonOperator(
    task_id = 'extract_csv_data',
    python_callable = extract_csv_data,
    dag = dag,
)

# Tâche de transformation des données
transform_csv_data_task = PythonOperator(
    task_id = 'transform_csv_data',
    python_callable = transform_csv_data,
    provide_context = True, # permet de passer les données de la tâche précédente à la tâche suivante
    dag = dag,
)

# Tâche de Chargement des données
load_csv_data_task = PythonOperator(
    task_id = 'load_csv_data',
    python_callable = load_csv_data,
    provide_context = True,
    dag = dag,
)

# # Extraction des données json
# def extract_json():
#     return load_json_file('../../data/intermediate/pubmed.json')

# extract_json_task = PythonOperator(
#     task_id = 'extract_pubmed_json',
#     python_callable = extract_json,
#     dag = dag,
# )

extract_csv_data_task >> transform_csv_data_task >> load_csv_data_task   # on relie les deux tâches ensemble
