import json
import os

# Fonction pour écrire les données dans un fichier json
def generate_output_file(mentions, file_path):
    os.makedirs(os.path.dirname(file_path), exist_ok=True) # creation du dossier de destination du fichier de sortie (s'il n'existe pas
    with open(file_path, 'w', encoding='utf-8') as file:
        json.dump(mentions, file, ensure_ascii=False, indent=4)