import re

# cette fonction permet de vérifier si le nom du médicament est mentionné dans le titre
# si oui, elle retourne True, sinon elle retourne False
def is_mention(drug_name, title):
    return re.search(drug_name, title, re.IGNORECASE) is not None

# cette fonction permet de trouver les mentions d'un médicament dans une liste d'articles
# elle retourne une liste de dictionnaires contenant les mentions trouvées
def find_mention(articles, drugs):
    mentions = []
    for drug in drugs:
        for article in articles:
            title = article.get('title', '')
            if is_mention(drug['drug'],title): # drug['drug'] permet de 
                mention = { # creation d'un dictionnaire pour stocker les informations de la mention
                    'drug': drug['drug'],
                    'title': title,
                    'date': article.get('date', ''),
                    'journal': article.get('journal', ''),
                }
                mentions.append(mention) # Ajout du dictionnaire dans la liste des mentions
    return mentions
