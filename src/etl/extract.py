# Chargement des données des fichiers dans des structures de données
# -----------------------------------------------------------------

import csv
import json

# Fonction pour charger les données d'un fichier csv dans une liste de dictionnaires
def load_csv_file(file_path):
    data = []
    with open(file_path, 'r', encoding='utf-8') as file:
# cette ligne permet de lire le fichier csv et de le convertir en dictionnaire et le stocker dans la variable reader
        reader = csv.DictReader(file)
        for row in reader:
            data.append(row)
    return data

# Fonction pour charger les données d'un fichier json dans une liste de dictionnaires
def load_json_file(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        return json.load(file)
# pourquoi on a pas besoin de boucle for pour lire le fichier json et le convertir en dictionnaire ?
# parce que la fonction json.load() fait le travail pour nous
# elle lit le fichier json et le convertit en dictionnaire

