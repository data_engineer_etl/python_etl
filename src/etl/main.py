import extract as ex
import transform as tr
import load as ld
import os

print(os.getcwd())

# Chargement des données des fichiers dans des structures de données
drugs = ex.load_csv_file('../../data/intermediate/drugs.csv')
pubmed = ex.load_csv_file('../../data/intermediate/pubmed.csv')
#pubmed_json = ex.load_json_file('../../data/gold/pubmed.json')
clinical_trials = ex.load_csv_file('../../data/intermediate/clinical_trials.csv')

# Transformation des données
# Trouver les mentions des médicaments dans les articles
pubmed_mentions = tr.find_mention(pubmed, drugs)
clinical_trials_mentions = tr.find_mention(clinical_trials, drugs)
#pubmed_json_mentions = tr.find_mentions(pubmed_json, drugs)

# Combiner les mentions trouvées dans une seule liste
mentions = pubmed_mentions + clinical_trials_mentions #+ pubmed_json_mentions

# Chargement des données transformées dans le fichier de sortie
ld.generate_output_file(mentions, '../../data/gold/mentions.json')
